package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		return team;
		//return null;
	}

	@Override
	public String toString(){
		return this.getName();
	}

	@Override
	public boolean equals(Object other){
		if (other == null) { return false; }
		if (!(other instanceof Team)) {
			return false;
		}
		return this.getName().equals(((Team)other).getName());
	}

}
