package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String FIND_ALL_USERS = "SELECT * FROM users";
	private static final String INSERT_USER = "INSERT INTO users(id, login) VALUES (DEFAULT, ?)";
	private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
	private final static String INSERT_TEAM = "INSERT INTO teams(id, name) VALUES (DEFAULT, ?)";
	private final static String GET_USER = "SELECT id FROM users WHERE login = ?";
	private final static String GET_TEAM = "SELECT id FROM teams WHERE name = ?";
	private final static String SET_TEAM_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
	private final static String GET_USER_TEAMS = "SELECT teams.id, teams.name FROM teams JOIN users_teams ON teams.id = team_id WHERE user_id = ?" ;
	private final static String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
	private final static String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
	private final static String DELETE_USER = "DELETE FROM users WHERE id = ?";

	private static final String DATABASE_URL;
	private static final Properties properties = new Properties();

	private static DBManager instance;
	private static Connection connection;

	static {
		try {
			properties.load(new FileReader("app.properties"));
		} catch (IOException e){
			e.printStackTrace();
		}
		DATABASE_URL = properties.getProperty("connection.url");
	}

	public static synchronized DBManager getInstance() {

		if (instance == null) {
			try { instance = new DBManager(); }
			catch (Exception ex){ex.printStackTrace();}
		}
		return instance;
	}

	private DBManager() {
		Properties properties = new Properties();
		try (FileReader fileReader = new FileReader("app.properties")) {
			properties.load(fileReader);
		}
		catch (Exception e){
			System.err.println("Could not read properties file " + e.getMessage());
			System.exit(-1);
		}
		try {
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			//System.out.println(properties.getProperty("connection.url"));
		}
		catch (Exception e){
			System.err.println("Could not connect, reason " + e.getMessage());
			System.exit(-1);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> result = new ArrayList<>();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_USERS);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()){
				//System.out.println("in while");
				User user = new User();
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));
				//System.out.println(user.getLogin());
				result.add(user);
			}
		}
		catch (SQLException sqlException){
			System.err.println("Error find all users");
			throw new DBException("Error conn", sqlException);
		}

		return result;
	}

	public boolean insertUser(User user) throws DBException {
		int result = -1;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getLogin());
			result = preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while (resultSet.next()){
				user.setId(resultSet.getInt(1));
			}
		}
		catch (SQLException sqlException){
			System.err.println(sqlException.getMessage());
			throw new DBException("Error conn / insert user", sqlException);
		}
		return result == 1 ? true : false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try{
			for(User user : users) {
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER);
				preparedStatement.setString(1, String.valueOf(user.getId()));
				preparedStatement.execute();
			}
		}
		catch (SQLException sqlException){
			System.err.println(sqlException.getMessage());
			throw new DBException("Error conn / delete user", sqlException);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		ResultSet resultSet = null;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(GET_USER);
			preparedStatement.setString(1, login);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(login);
			}
		}
		catch (SQLException sqlException){
			throw new DBException("Error conn / insert user", sqlException);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		ResultSet resultSet = null;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(GET_TEAM);
			preparedStatement.setString(1, name);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(name);
			}
		}
		catch (SQLException sqlException){
			throw new DBException("Error conn / insert user", sqlException);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> result = new ArrayList<>();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_TEAMS);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()){
				//System.out.println("in while");
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				//System.out.println(user.getLogin());
				result.add(team);
			}
		}
		catch (SQLException sqlException){
			System.err.println("Error find all teams");
			throw new DBException("Error conn", sqlException);
		}
		return result;
	}

	public boolean insertTeam(Team team) throws DBException {
		int result = -1;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, team.getName());
			result = preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while (resultSet.next()){
				team.setId(resultSet.getInt(1));
			}
		}
		catch (SQLException sqlException){
			throw new DBException("Error conn / insert user", sqlException);
		}
		return result == 1 ? true : false ;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		boolean success = false;

		if (user == null) { return false; }
		for(Team t : teams){
			if (t == null) { return false; }
		}

		Connection con = null;
		PreparedStatement preparedStatement = null;

		try {
			con = DriverManager.getConnection(DATABASE_URL);
			con.setAutoCommit(false);
			preparedStatement = con.prepareStatement(SET_TEAM_FOR_USER);

			for (Team team : teams) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				//preparedStatement.setString(1, Integer.toString(user.getId()));
				//preparedStatement.setString(2, Integer.toString(team.getId()));
				preparedStatement.executeUpdate();
				//connection.commit();
			}
			con.commit();
			success = true;
		}

		catch (SQLException e){
			//rollback(con);
			try {
				con.rollback();
				System.err.println(e.getMessage());
				//throw new DBException("Err", e);
			}
			catch (Exception exception){
				System.err.println(exception.getStackTrace());
			}
			throw new DBException("DBException set teams for user", e);
		}
		finally {
			try{
				con.close();
				preparedStatement.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> result = new ArrayList<>();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_TEAMS);
			preparedStatement.setString(1, String.valueOf(user.getId()));
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()){
				//System.out.println("in while");
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				//System.out.println(user.getLogin());
				result.add(team);
			}
		}
		catch (SQLException sqlException){
			System.err.println("Error in method getUsersTeams " + sqlException.getMessage() + " ");
			throw new DBException("Error conn", sqlException);
		}
		return result;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean result = false;
		try{
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TEAM);
			preparedStatement.setString(1, String.valueOf(team.getId()));
			result = preparedStatement.execute();
		}
		catch (SQLException sqlException){
			System.err.println(sqlException.getMessage());
			throw new DBException("Error conn / delete team", sqlException);
		}
		return result;
	}

	public boolean updateTeam(Team team) throws DBException {

		try{
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TEAM);
			preparedStatement.setString(1, team.getName());
			preparedStatement.setString(2, String.valueOf(team.getId()));
			preparedStatement.execute();
		}
		catch (SQLException sqlException){
			System.err.println(sqlException.getMessage());
			throw new DBException("Error conn / update team", sqlException);
		}

		return true;
	}

}
